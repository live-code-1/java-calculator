# Java Course | #02 Java Programming - Calculator with source (BASIC)

[![Java Programming Lesson 2](http://img.youtube.com/vi/TI0ESmIoufs/0.jpg)](https://youtu.be/TI0ESmIoufs)

*__Kindly Like, Subscribe and Share the YouTube Video__*  

## Java Course - Lesson 2

Welcome to Java Course (Lesson 2), in this session, we will develop a simple Java calculator. We can learn the Java
Syntax, Comments, Variable & Data Type, Operators, also the printing method and switch-case statement. Wish you guys can
have a basic development concept of Java.  

*Feel free to comment and let us know what kind video you would like to watch*  

## Exercise Answer

```
switch (operator) {    
    ...

    case "M": // Multiplication
        System.out.printf("Result of %s multiple %s is %s %n", firstNum, secondNum, (firstNum * secondNum));
        break;
    case "D": // Division
        System.out.printf("Result of %s divide %s is %s %n", firstNum, secondNum, (firstNum / secondNum));
        break;

    ...
}
```
